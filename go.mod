module gitlab.com/yawning/obfs4.git

require (
	filippo.io/edwards25519 v1.0.0
	git.torproject.org/pluggable-transports/goptlib.git v1.3.0
	github.com/dchest/siphash v1.2.3
	gitlab.com/yawning/edwards25519-extra.git v0.0.0-20211229043746-2f91fcc9fbdb
	golang.org/x/crypto v0.9.0
	golang.org/x/net v0.10.0
)

go 1.20
